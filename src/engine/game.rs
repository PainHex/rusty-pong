extern crate gfx;

pub trait Game {

    fn update(&mut self) {
    }

    fn render(&mut self) {

    }
}