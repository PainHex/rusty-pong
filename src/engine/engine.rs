extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;
extern crate time;
extern crate gfx_core;
extern crate cgmath;
extern crate gfx_device_gl;

use gfx::Device;
use gfx::traits::FactoryExt;
use gfx_core::command::ClearColor;
use cgmath::*;
use std::collections::HashSet;
use glutin::VirtualKeyCode as KeyCode;

use engine::math::vec::IVec2;
use engine::game::Game;
use engine::renderer::renderer::GraphicsContext;
use engine::renderer::shader::shader_data::*;
use game::player::player::Player;
use game::player::camera::Camera;
use game::global_traits::commandable::Commandable;
use game::terrain::terrain::Terrain;

pub struct Engine<G: Game> {
    pub running: bool,
    frame_time: f64,
    current_frame_time: f64,
    last_frame_time: f64,
    window_size: (i32, i32),
    keys: HashSet<KeyCode>,
    game: G,
    terrain: Terrain,
    player: Player,
}

fn current_time_in_millis() -> f64 {
    let current_time = time::get_time();
    (current_time.sec as f64 * 1000.0) + (current_time.nsec as f64 / 1000.0 / 1000.0)
}

impl <G: Game> Engine<G> {

    pub fn new(game: G) -> Engine<G> {
        let mut engine = Engine {
            running: true,
            frame_time: 1000.0/60.0,
            current_frame_time: current_time_in_millis(),
            last_frame_time: current_time_in_millis(),
            window_size: (1024, 768),
            keys: HashSet::new(),
            game: game,
            terrain: Terrain::new(Vector2::new(128, 16), Vector2::new(0.0, 0.0)),
            player: Player::new(Vector2::new(0.0, 0.0)),
        };

        engine
    }

    pub fn running(&self) -> bool {
        return self.running;
    }

    pub fn handle_events(&self) {

    }

    pub fn render(&self) {

    }

    pub fn update(&self) {

    }

    pub fn run(&mut self) {
        let events_loop = glutin::EventsLoop::new();
        let builder = glutin::WindowBuilder::new()
            .with_title("Terraformia".to_string())
            .with_dimensions(self.window_size.0 as u32, self.window_size.1 as u32)
            .with_vsync();

        let (window, mut device, mut factory, main_color, mut main_depth) =
            gfx_window_glutin::init::<ColorFormat, DepthFormat>(builder, &events_loop);


        let mut encoder: gfx::Encoder<_, _> = factory.create_command_buffer().into();
        let pso = factory.create_pipeline_simple(
            include_bytes!("../../assets/shader/vertex.glsl"),
            include_bytes!("../../assets/shader/frag.glsl"),
            pipe::new()
        ).unwrap();

        let (vertices, indices) = self.terrain.get_vertex_data();
        let (vertex_buffer, mut slice) = factory.create_vertex_buffer_with_slice(&vertices, &*indices);
        let projection = cgmath::ortho(0.0, self.window_size.0 as f32, self.window_size.1 as f32, 0.0, -1.0, 1.0);

        let mut data = pipe::Data {
            projection: projection.into(),
            view: self.player.view_matrix().into(),
            vbuf: vertex_buffer,
            out: main_color,
        };

        while self.running {
            data.view = self.player.view_matrix().into();
            self.current_frame_time = current_time_in_millis();
            let mut frame_delta = self.current_frame_time - self.last_frame_time;
            let mut updates = 0;

            while frame_delta > self.frame_time && updates < 10 {
                frame_delta -= self.frame_time;
                updates += 1;
                self.player.update();
            }
            self.last_frame_time = self.current_frame_time;
            events_loop.poll_events(|glutin::Event::WindowEvent { window_id: _, event }| {
                match event {
                    glutin::WindowEvent::KeyboardInput(_, _, Some(glutin::VirtualKeyCode::Escape), _) |
                    glutin::WindowEvent::Closed => self.running = false,
                    glutin::WindowEvent::KeyboardInput(_, _, key, _) => {
                        self.player.process_command(key.unwrap());
                    },
                    glutin::WindowEvent::Resized(width, height) => {
                        gfx_window_glutin::update_views(&window, &mut data.out, &mut main_depth);
                        self.terrain.update_ratio(width as f32 / height as f32);
                        self.window_size = (width as i32, height as i32);
                    }
                    glutin::WindowEvent::MouseMoved(x, y) => {
                    },
                    glutin::WindowEvent::MouseInput(glutin::ElementState::Pressed, glutin::MouseButton::Left) => {

                    },
                    glutin::WindowEvent::MouseInput(glutin::ElementState::Released, glutin::MouseButton::Left) => {

                    },
                    _ => {}
                }
            });

            let (vertices, indices) = self.terrain.get_vertex_data();
            let (vertex_buffer, s1) = factory.create_vertex_buffer_with_slice(&vertices, &*indices);

            data.vbuf = vertex_buffer;
            slice = s1;

            encoder.clear(&data.out, [0.0, 0.0, 0.0, 0.0]);
            encoder.draw(&slice, &pso, &data);
            encoder.flush(&mut device);
            window.swap_buffers().unwrap();
            device.cleanup();
        }
    }
}