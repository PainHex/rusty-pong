extern crate gfx;
extern crate glutin;
extern crate gfx_device_gl;

use std::fmt;

use engine::renderer::shader::shader_data::*;

pub struct GraphicsContextGeneric<R, F, C, D>
    where
        R: gfx::Resources,
        F: gfx::Factory<R>,
        C: gfx::CommandBuffer<R>,
        D: gfx::Device<Resources=R, CommandBuffer=C>,
{
    device: Box<D>,
    factory: Box<F>,
    encoder: gfx::Encoder<R, C>,
}

impl<R, F, C, D> fmt::Debug for GraphicsContextGeneric<R, F, C, D>
    where
        R: gfx::Resources,
        F: gfx::Factory<R>,
        C: gfx::CommandBuffer<R>,
        D: gfx::Device<Resources=R, CommandBuffer=C>,
{

    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "<GraphicsContext: {:p}>", self)
    }
}

pub type GraphicsContext = GraphicsContextGeneric<
    gfx_device_gl::Resources,
    gfx_device_gl::Factory,
    gfx_device_gl::CommandBuffer,
    gfx_device_gl::Device,
>;

impl GraphicsContext {
    pub fn new() -> GraphicsContext {
        GraphicsContext {

        }
    }
}