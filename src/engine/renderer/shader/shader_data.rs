extern crate gfx;

pub type ColorFormat = self::gfx::format::Rgba8;
pub type DepthFormat = self::gfx::format::DepthStencil;

pub const WHITE: [f32; 3] = [1.0, 1.0, 1.0];

gfx_defines! {
    vertex Vertex {
        pos: [f32; 2] = "a_Pos",
        color: [f32; 3] = "a_Color",
    }

    pipeline pipe {
        vbuf: gfx::VertexBuffer<Vertex> = (),
        projection: gfx::Global<[[f32; 4]; 4]> = "projection",
        view: gfx::Global<[[f32; 4]; 4]> = "view",
        out: gfx::RenderTarget<ColorFormat> = "Target0",
    }
}