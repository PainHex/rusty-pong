#[macro_use]
extern crate gfx;
extern crate gfx_window_glutin;
extern crate glutin;
extern crate cgmath;
extern crate gfx_core;

pub mod game;
pub mod engine;

use engine::engine::Engine as Engine;
use game::terraformia::Terraformia as Terraformia;

pub fn run() {
    Engine::new(Terraformia::new()).run();
}
