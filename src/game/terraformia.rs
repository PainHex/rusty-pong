extern crate gfx;
extern crate cgmath;

use cgmath::*;

use engine::engine::Engine as GameEngine;
use game::terrain::terrain::*;
use engine::game::Game;
use game::player::player::Player;

pub struct Terraformia {
    terrain: Terrain,
    player: Player,
}

impl Terraformia {
    pub fn new() -> Terraformia {
        Terraformia {
            terrain: Terrain::new(Vector2::new(128, 16), Vector2::new(0.0, 0.0)),
            player: Player::new(Vector2::new(0.0, 0.0)),
        }
    }
}

impl Game for Terraformia {

}