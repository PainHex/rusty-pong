pub trait Commandable<T> {

    fn process_command(&mut self, command: T);

    fn update(&mut self);

}