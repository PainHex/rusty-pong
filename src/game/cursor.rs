#[derive(Debug, Clone, Copy)]
pub enum Cursor {
    PLAIN((f32, f32), [f32; 3]),
    GROWING((f32, f32), f32, [f32; 3])
}

impl Cursor {
}
