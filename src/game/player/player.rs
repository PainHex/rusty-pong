extern crate cgmath;
extern crate glutin;

use cgmath::*;

use super::camera::*;
use game::global_traits::commandable::Commandable;

pub struct Player {
    position: Vector2<f32>,
    speed: Vector2<f32>,
}

impl Player {

    pub fn new(position: Vector2<f32>) -> Player {
        Player {
            position: position,
            speed: Vector2::new(0.0, 0.0),
        }
    }

}

impl Camera for Player {

    fn view_matrix(&mut self) -> Matrix4<f32> {
        Matrix4::look_at(
            cgmath::Point3::new(self.position.x, self.position.y, 1.0),
            cgmath::Point3::new(self.position.x, self.position.y, 0.0),
            cgmath::Vector3::new(0.0, 1.0, 0.0),
        )
    }

}

impl Commandable<glutin::VirtualKeyCode> for Player {

    fn process_command(&mut self, command: glutin::VirtualKeyCode) {
        match command {
            glutin::VirtualKeyCode::A => {
                self.speed = self.speed + Vector2::new(-0.1, 0.0);
            },
            glutin::VirtualKeyCode::D => {
                self.speed = self.speed + Vector2::new(0.1, 0.0);
            },
            glutin::VirtualKeyCode::W => {
                self.speed = self.speed + Vector2::new(0.0, 0.1);
            },
            glutin::VirtualKeyCode::S => {
                self.speed = self.speed + Vector2::new(0.0, -0.1);
            },
            _ => {}
        }

        self.speed = Vector2::new(self.speed.x.min(0.1), self.speed.y.min(0.1));
        self.speed = Vector2::new(self.speed.x.max(-0.1), self.speed.y.max(-0.1));
    }

    fn update(&mut self) {
        self.position += self.speed;
    }
}
