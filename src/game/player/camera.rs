extern crate cgmath;

use cgmath::Matrix4;

pub trait Camera {

    fn view_matrix(&mut self) -> Matrix4<f32>;

}