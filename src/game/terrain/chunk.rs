extern crate cgmath;
extern crate gfx;
extern crate rand;

use cgmath::Vector2;
use engine::renderer::shader::shader_data::*;

const SQUARE_SIZE: f32 = 1.0;

#[derive(Debug, Copy, Clone)]
struct Tile {
    filled: bool
}

#[derive(Debug)]
pub struct Chunk {
    position: Vector2<f32>,
    size: Vector2<u8>,
    tiles: [Tile; 0]
}

impl Chunk {

    pub fn new(size: Vector2<u8>, position: Vector2<f32>) -> Self {
        Chunk {
            size: size,
            position: position,
            tiles: []
        }
    }

    pub fn get_vertex_data(&self) -> (Vec<Vertex>, Vec<u32>) {
        let (mut vertices, mut indices) = (vec![], vec![]);

        let half = 0.5 * SQUARE_SIZE;
        let mut i = 0 as u32;
        for x_index in 0..self.size.x {
            for y_index in 0..self.size.y {
                let (x_pos, y_pos) = (x_index as f32 * SQUARE_SIZE + self.position.x, y_index as f32 * SQUARE_SIZE + self.position.y);

                vertices.extend(&[
                    Vertex { pos: [x_pos + half, y_pos - half], color: WHITE },
                    Vertex { pos: [x_pos - half, y_pos - half], color: WHITE },
                    Vertex { pos: [x_pos - half, y_pos + half], color: WHITE },
                    Vertex { pos: [x_pos + half, y_pos + half], color: WHITE },
                ]);
                indices.extend(&[
                    4 * i, 4 * i + 1, 4 * i + 2, 4 * i + 2, 4 * i + 3, 4 * i
                ]);
                i += 1;
            }
        }

        (vertices, indices)
    }
}