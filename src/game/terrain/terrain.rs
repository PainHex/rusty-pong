extern crate cgmath;
extern crate gfx;
extern crate rand;

use cgmath::Vector2;
use engine::renderer::shader::shader_data::*;
use game::terrain::chunk::*;

#[derive(Debug)]
pub struct Terrain {
    size: Vector2<u8>,
    position: Vector2<f32>,
    ratio: f32,
    chunk: Chunk,
}

impl Terrain {

    pub fn new(size: Vector2<u8>, position: Vector2<f32>) -> Self {
        Terrain {
            size: size,
            position: position,
            ratio: 1.0,
            chunk: Chunk::new(size, position),
        }
    }

    pub fn get_vertex_data(&mut self) -> (Vec<Vertex>, Vec<u32>) {
        self.chunk.get_vertex_data()
    }

    pub fn update_ratio(&mut self, ratio: f32) {
        self.ratio = ratio;
    }

}